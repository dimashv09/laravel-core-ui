<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-toggle="sidebar-show"
    data-target="#sidebar" data-class="c-sidebar-minimized">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>

<ul class="c-header-nav ml-auto mr-4">
    <li class="c-header-nav-item d-md-down-none mx-2">
        <a class="c-header-nav-link" href="#">
            <i class="c-icon cil-bell"></i>
        </a>
    </li>
    <li class="c-header-nav-item d-md-down-none mx-2">
        <a class="c-header-nav-link" href="#">
            <i class="c-icon cil-list-rich"></i>
        </a>
    </li>
    <li class="c-header-nav-item d-md-down-none mx-2">
        <a class="c-header-nav-link" href="#">
            <i class="c-icon cil-envelope-open"></i>
        </a>
    </li>
    <li class="c-header-nav-item dropdown">
        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
            aria-expanded="false">
            <!-- <div class="c-avatar"><img class="c-avatar-img" src="assets/img/avatars/6.jpg" alt="user@email.com"></div> -->
            <i class="c-icon cil-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2">
                <strong>Account</strong>
            </div>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-bell"></i>
                Updates<span class="badge badge-info ml-auto">42</span>
            </a>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-envelope-open"></i>
                Messages<span class="badge badge-success ml-auto">42</span>
            </a>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-task"></i>
                Tasks<span class="badge badge-danger ml-auto">42</span>
            </a>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-comment-square"></i>
                Comments<span class="badge badge-warning ml-auto">42</span>
            </a>

            <div class="dropdown-header bg-light py-2">
                <strong>Settings</strong>
            </div>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-user"></i>
                Profile
            </a>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-settings"></i>
                Settings
            </a>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-credit-card"></i>
                Payments<span class="badge badge-secondary ml-auto">42</span>
            </a>
            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-file"></i>
                Projects<span class="badge badge-primary ml-auto">42</span>
            </a>

            <div class="dropdown-divider"></div>

            <a class="dropdown-item" href="#">
                <i class="c-icon mr-2 cil-lock-locked"></i>
                Lock Account
            </a>
            <form method="POST" action="#">
                @csrf
                <button class="dropdown-item" type="submit">
                    <i class="c-icon mr-2 cil-account-logout"></i>
                    Logout
                </button>
            </form>
        </div>
    </li>
</ul>
