<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="{{ asset('/assets/favicon/favicon.ico') }}">
    {{-- Lato --}}
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,300italic,400italic" rel="stylesheet">

    <!-- CoreUI CSS -->
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.5.0/css/perfect-scrollbar.min.css"
        integrity="sha512-n+g8P11K/4RFlXnx2/RW1EZK25iYgolW6Qn7I0F96KxJibwATH3OoVCQPh/hzlc4dWAwplglKX8IVNVMWUUdsw=="
        crossorigin="anonymous" />

    <title>{{ config('app.name', 'Laravel Core UI') }}</title>

    <script src="{{ mix('js/admin.js') }}" defer></script>

    @stack('styles')
</head>

<body class="c-app">
    @include('admin.partials.sidebar')

    <div class="c-wrapper c-fixed-components">
        <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
            @include('admin.partials.header')

            <div class="c-subheader px-3">
                <!-- Breadcrumb-->
                <ol class="breadcrumb border-0 m-0">
                    @isset($breadcrumb)
                    @if(isset($child))
                    <li class="breadcrumb-item"><a href="{{ route($route) }}">{{ $breadcrumb }}</a></li>
                    <li class="breadcrumb-item active">{{ $child['title'] }}</li>
                    @else
                    <li class="breadcrumb-item active">{{ $breadcrumb }}</li>
                    @endif
                    @endisset
                </ol>
            </div>
        </header>
        <div class="c-body">
            <main class="c-main">
                @if (session()->has('response'))
                <div class="body flex-grow-1 px-3">
                    <div class="alert alert-{{ session('response.class') }}">
                        {{ session('response.message') }}
                    </div>
                </div>
                @endif

                @yield('content')
            </main>

            <footer class="c-footer">
                <div>Copyright &copy; {{ date('Y') }} Lara Core UI. All rights reserved.</div>
            </footer>
        </div>
    </div>

    @stack('scripts')
</body>

</html>
