@extends('admin.layouts.app', [
    'breadcrumb' => "User",
    'route' => "admin.users.index",
    'child' => [
        'title' => "Add User",
        'route' => "admin.users.create"
        ]
])


@section('content')

<div class="body flex-grow-1 px-3">
    <div class="card">
        <div class="card-header">
            Add User
        </div>
        <div class="card-body">
            <livewire:admin.users.form :id="0"/>
        </div>
    </div>
</div>

@stop


@push('styles')
    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet">
@endpush
