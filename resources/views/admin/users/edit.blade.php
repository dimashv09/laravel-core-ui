@extends('admin.layouts.app', [
    'breadcrumb' => "User",
    'route' => "admin.users.index",
    'child' => [
        'title' => "Update User",
        'route' => "admin.users.edit"
        ]
])


@section('content')

<div class="body flex-grow-1 px-3">
    <div class="card">
        <div class="card-header">
            Update User
        </div>
        <div class="card-body">
            <livewire:admin.users.form :edit="$edit" :id="$id"/>
        </div>
    </div>
</div>

@stop


@push('styles')
    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet">
@endpush
