@extends('admin.layouts.app', [
    'breadcrumb' => "Users",
    'route' => "admin.users.index",
])


@section('content')
<div class="body flex-grow-1 px-3">
    <livewire:admin.users.user-datatable />
</div>
@stop

@push('styles')
    <link href="{{ mix('css/tailwind.css') }}" rel="stylesheet">
@endpush
