<div class="mb-3">
    <div class="grid grid-cols-1 gap-1 mb-3">
        <div class="text-xl font-bold">
            User List
        </div>
    </div>
    <div class="grid grid-cols-6 gap-6">
        <form wire:submit.prevent="bulk_selected" class="col-span-8 sm:col-span-2">
            <div class="grid grid-cols-2 gap-2">
                <div class="col-span-8 sm:col-span-1">
                    <select wire:model="bulk_option" class="block w-full py-2 px-4 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                        <option value="" selected>Choose Action</option>
                        <option value="delete">Delete</option>
                    </select>
                </div>
                <div class="col-span-4 sm:col-span-1">
                    <x-buttons.button-secondary
                        type="submit">
                        Apply
                    </x-buttons.button-secondary>
                </div>
            </div>
        </form>
        <div class=" text-right col-span-4 sm:col-span-4">
            <x-buttons.button-link-primary
                href="{{ route('admin.users.create') }}"
                type="button">
                Add User
            </x-buttons.button-link-primary>
        </div>
    </div>
</div>
